import QtQuick 2.11

Item {
    property string url: ""
    width: 1920
    height: 1080

    anchors.fill: parent

    GridView {
        id: grid
        width: 1200
        height: 300
        anchors.verticalCenterOffset: 200
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        focus: true

        cellWidth: 300
        cellHeight: 150

        model: streamChannelsModel

        delegate: ChannelButton {
            state: GridView.isCurrentItem ? "selected" : ""
            src: imageSrc
        }

        onCurrentIndexChanged: url = this.model.get(this.currentIndex).streamUrl
    }


    ListModel {
        id: streamChannelsModel

        ListElement {
            imageSrc: "qrc:/img/main.png"
            streamUrl: "https://moiptvhls-i.akamaihd.net/hls/live/652042/secure/index.m3u8"
        }

        ListElement {
            imageSrc: "qrc:/img/proc.png"
            streamUrl: "https://moiptvhls-i.akamaihd.net/hls/live/652317/secure/master.m3u8"
        }

        ListElement {
            imageSrc: "qrc:/img/dare.png"
            streamUrl: "https://moiptvhls-i.akamaihd.net/hls/live/652313/secure/master.m3u8"
        }

        ListElement {
            imageSrc: "qrc:/img/int.png"
            streamUrl: "https://moiptvhls-i.akamaihd.net/hls/live/652312/secure/master.m3u8"
        }

        ListElement {
            imageSrc: "qrc:/img/kids.png"
            streamUrl: "https://moiptvhls-i.akamaihd.net/hls/live/652318/secure/master.m3u8"
        }

        ListElement {
            imageSrc: "qrc:/img/lat.png"
            streamUrl: "https://moiptvhls-i.akamaihd.net/hls/live/652315/secure/master.m3u8"
        }

        ListElement {
            imageSrc: "qrc:/img/fr.png"
            streamUrl: "https://moiptvhls-i.akamaihd.net/hls/live/652314/secure/master.m3u8"
        }

        ListElement {
            imageSrc: "qrc:/img/ru.png"
            streamUrl: "https://moiptvhls-i.akamaihd.net/hls/live/652316/secure/master.m3u8"
        }
    }
    // ListModel END

}

