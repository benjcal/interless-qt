import QtQuick 2.11
import QtGraphicalEffects 1.0

Item {
    property alias state: rectangle.state
    property alias src: image.source

    id: channelButton
    width: 300
    height: 150
    opacity: 0.3

    Rectangle {
        id: rectangle
        width: 260
        height: 130
        color: "#00ffffff"
        radius: 16
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter


        Image {
            id: image
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            antialiasing: true
        }

        transitions: [
            Transition {

                NumberAnimation {
                    target: channelButton
                    property: "opacity"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }

            }
        ]

        states: [
            State {
                name: "selected"

                PropertyChanges {
                    target: rectangle
                    color: "#90ffffff"
                }

                PropertyChanges {
                    target: channelButton
                    opacity: 1
                }
            }
        ]
    }

    DropShadow {
            anchors.fill: rectangle
            horizontalOffset: 3
            verticalOffset: 3
            radius: 8.0
            samples: 17
            color: "#80000000"
            source: rectangle
        }

}


