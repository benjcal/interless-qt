import QtQuick 2.11
import QtGraphicalEffects 1.0

Item {
    property alias state: rectangle.state
    property alias iconCode: icon.text
    property alias iconName: name.text

    id: homeButton
    width: 300
    height: 150
    opacity: 0.3

    Rectangle {
        id: rectangle
        width: 260
        height: 150
        color: "#00ffffff"
        radius: 16
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        border.width: 4
        border.color: "#00ffffff"


        Text {
            id: icon
            color: "#ffffff"
            text: "\uf144"
            anchors.verticalCenterOffset: -27
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 32

        }

        Text {
            id: name
            x: 118
            y: 81
            color: "#ffffff"
            anchors.verticalCenterOffset: 32
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 30
        }

        transitions: [
            Transition {

                NumberAnimation {
                    target: homeButton
                    property: "opacity"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }

            }
        ]

        states: [
            State {
                name: "selected"

                PropertyChanges {
                    target: rectangle
                    border.color: "#90ffffff"
                }

                PropertyChanges {
                    target: homeButton
                    opacity: 1
                }
            }
        ]
    }

    DropShadow {
        id: dropShadow
        anchors.fill: rectangle
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 17
        color: "#80000000"
        source: rectangle
    }

}


