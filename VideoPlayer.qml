import QtQuick 2.11
import QtMultimedia 5.9

Rectangle {
    property alias url: video.source

    anchors.fill: parent
    color: "black"

    Video {


        id: video
        focus: true
        anchors.fill: parent


        autoPlay: true

        Keys.onSpacePressed: video.playbackState === MediaPlayer.PlayingState ? video.pause() : video.play()

    }

}

