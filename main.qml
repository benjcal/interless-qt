import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.2

Window {
    /* Global state */
    property string currentVideoUrl: ""

    visible: true

    /* 1080p */
    width: 1920
    height: 1080

    /* temporary background */
    Rectangle {
        color: "#3b3b3b"
        anchors.fill: parent
    }

    Image {
        id: image
        width: 320
        height: 260
        anchors.horizontalCenter: parent.horizontalCenter
        fillMode: Image.PreserveAspectFit
        source: "qrc:/img/interless_logo.svg.png"
    }

    /* Main Stack View
        this is in charge of view transition and global navigation */
    StackView {
        focus: true
        id: mainStack
        anchors.fill: parent

        Keys.onPressed: function(e) { handleKeys(e) }

        initialItem: home

    }

    /*** Component loaders START
        these components are loaded to be ready to be pushed to the main stack */

    Component {
        id: home
        Loader {
            source: "qrc:/components/Home.qml"
            asynchronous: true
        }
    }

    /* main video player
        take the url from context */
    Component {
        id: video
        Loader {
            source: "qrc:/components/VideoPlayer.qml"
            asynchronous: true
            onLoaded: {
                item.url = currentVideoUrl
            }
        }
    }

    Component {
        id: stream
        Loader {
            source: "qrc:/components/StreamChannels.qml"
            asynchronous: true
        }
    }
    /*** Component loaders END */



    /*** Functions that make us of the context */

    /* handle keys globally here */
    function handleKeys(e) {
        switch (e.key) {
            case Qt.Key_Escape:
                Qt.quit()
                break
            case Qt.Key_Return:
                if (mainStack.currentItem.item.url) {
                    currentVideoUrl = mainStack.currentItem.item.url
                    actions("PLAY")
                }
                if (mainStack.currentItem.item.selection) {
                    actions("SELECT")
                }
                break
            case Qt.Key_Home:
                actions("HOME")
                break
            case Qt.Key_Backspace:
                actions("BACK")
                break

        }

    }


    /* actions WIP */
    function actions(a) {
        switch (a) {
            case "PLAY":
                mainStack.push(video)
                break
            case "HOME":
                mainStack.clear()
                mainStack.replace(home) // replace may introduce wrong behaviour
                break
            case "BACK":
                mainStack.pop()
                break
            case "SELECT":
                if (mainStack.currentItem.item.selection === "Stream") {
                    mainStack.push(stream)
                }
                break
            }
    }


}


