import QtQuick 2.11

Item {
    property string selection: ""
    width: 1920
    height: 1080

    anchors.fill: parent

    GridView {
        id: grid
        width: 1200
        height: 300
        anchors.verticalCenterOffset: 200
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        focus: true

        cellWidth: 300
        cellHeight: 150

        model: iconsModel


        delegate: HomeButton {
            state: GridView.isCurrentItem ? "selected" : ""
            iconCode: icon
            iconName: name
        }

        onCurrentIndexChanged: selection = this.model.get(this.currentIndex).name
    }

    ListModel {
        id: iconsModel

        ListElement {
            name: "Stream"
            icon: "\uf144"
        }

        ListElement {
            name: "Categories"
            icon: "\uf0cb"
        }

        ListElement {
            name: "Search"
            icon: "\uf002"
        }

        ListElement {
            name: "Options"
            icon: "\uf013"
        }
    }
    // ListModel END

}

